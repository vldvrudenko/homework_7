"use strict";

const list = ["day","night","bad","moon","121","23","user","country","boy","Vova","Alex",23];

const renderList = (arr, dbody = document.body) => {
  let li = arr.map((e) => `<li>${e}</li>`);
  let clearLi = li.join("");
  let ul = document.createElement("ul");
  ul.innerHTML = clearLi;
  dbody.prepend(ul);
};

renderList(list);

